#include "main.h"

ATOM RegMyWindowClass(HINSTANCE, LPCTSTR);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow) {
    LPCTSTR lpzClass = TEXT("Simple Window Application");
    if (!RegMyWindowClass(hInstance, lpzClass))
        return 1;
    application_struct applicationStruct;
    RECT screen_rect;
    GetWindowRect(GetDesktopWindow(), &screen_rect);
    int x = (screen_rect.right  - WINDOW_WIDTH) >> 1;
    int y = (screen_rect.bottom  - WINDOW_HEIGHT) >> 1;
    HWND hWnd = CreateWindow(lpzClass,
                             TEXT("Simple Application"),
                             WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                             x, y, WINDOW_WIDTH, WINDOW_HEIGHT,
                             NULL, NULL,
                             hInstance, NULL);
    createMenu(hWnd);
    // цикл сообщений приложения
    MSG msg = {0};    // структура сообщения
    int iGetOk = 0;   // переменная состояния
    while ((iGetOk = GetMessage(&msg, NULL, 0, 0)) != 0) // цикл сообщений
    {
        if (iGetOk == -1) return 3;  // если GetMessage вернул ошибку - выход
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;  // возвращаем код завершения программы
}

// функция регистрации класса окон
ATOM RegMyWindowClass(HINSTANCE hInst, LPCTSTR lpzClassName) {
    WNDCLASS wcWindowClass = {0};
    wcWindowClass.lpfnWndProc = (WNDPROC) WndProc;// адрес ф-ции обработки сообщений
    wcWindowClass.style = CS_HREDRAW | CS_VREDRAW;// стиль окна
    wcWindowClass.hInstance = hInst;  // дискриптор экземпляра приложения
    wcWindowClass.lpszClassName = lpzClassName; // название класса
    wcWindowClass.hCursor = LoadCursor(NULL, IDC_CROSS);   // загрузка курсора
    wcWindowClass.hbrBackground = (HBRUSH) COLOR_APPWORKSPACE;   // загрузка цвета окон
    return RegisterClass(&wcWindowClass); // регистрация класса
}



