#ifndef UNTITLED4_MAIN_H
#define UNTITLED4_MAIN_H

#include <windows.h>
#include <windowsx.h>
#include <wingdi.h>
#include <iostream>
#include "menu/menu.h"
#include "messagesHandler/messagesHandler.h"

#define PUSHED 1
#define UNPUSHED 0

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

struct application_struct{
    uint8_t brush;
};

#endif //UNTITLED4_MAIN_H
