#ifndef UNTITLED4_MESSAGESHANDLER_H
#define UNTITLED4_MESSAGESHANDLER_H

#include <wingdi.h>
#include <windows.h>

#define PUSHED 1
#define UNPUSHED 0

void UpdateWindow(HWND hWnd, LPRECT lprect);

LRESULT CALLBACK WndProc(HWND hWnd,
                         UINT message,
                         WPARAM wParam,
                         LPARAM lParam);

#endif //UNTITLED4_MESSAGESHANDLER_H
