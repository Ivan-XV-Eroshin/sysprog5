#include "messagesHandler.h"

void UpdateWindow(HWND hWnd, LPRECT lpRect) {
    GetWindowRect(hWnd, lpRect);
    InvalidateRect(hWnd, lpRect, false);
}

LRESULT CALLBACK WndProc(HWND hWnd,
                         UINT message,
                         WPARAM wParam,
                         LPARAM lParam) {
    POINT point;
    HDC hdc;
    PAINTSTRUCT paintStruct;
    RECT rect;
    switch (message) {
        case WM_PAINT: {
            if (wParam == PUSHED) {
                GetCursorPos(&point);
                MapWindowPoints(HWND_DESKTOP, hWnd, &point, 1);

                hdc = BeginPaint(hWnd, &paintStruct);
                SetPixel(hdc, point.x, point.y, RGB(0, 0, 0));
                SetPixel(hdc, point.x + 1, point.y + 2, RGB(0, 0, 0));
                SetPixel(hdc, point.x, point.y + 1, RGB(0, 0, 0));
                SetPixel(hdc, point.x + 2, point.y + 3, RGB(0, 0, 0));
                SetPixel(hdc, point.x + 2, point.y + 4, RGB(0, 0, 0));
                SetPixel(hdc, point.x - 1, point.y - 1, RGB(0, 0, 0));
                SetPixel(hdc, point.x - 1, point.y - 2, RGB(0, 0, 0));

                GetWindowRect(hWnd, &rect);
                ValidateRgn(hWnd, NULL);
                EndPaint(hWnd, &paintStruct);
                UpdateWindow(hWnd, &rect);
            }
            break;
        }
        case WM_MOUSEMOVE:
            if (GetAsyncKeyState(VK_LBUTTON)) {
                SendMessage(hWnd, WM_PAINT, PUSHED, 0);
            }
            break;
        case WM_LBUTTONUP:
            SendMessage(hWnd, WM_PAINT, UNPUSHED, 0);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);  // реакция на сообщение
            break;
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_DOWN: {
                    PostMessage(hWnd, WM_DESTROY, 0, 0);
                    break;
                }
                case VK_CONTROL:
                    while (GetAsyncKeyState(VK_CONTROL)) {
                        SendMessage(hWnd, WM_PAINT, PUSHED, 0);
                    }
                    break;
            }
            break;
        }
        default: {
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    return 0;
}