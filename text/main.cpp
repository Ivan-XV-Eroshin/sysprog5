#include <windows.h>
#include <iostream>
#include <windowsx.h>
#include <wingdi.h>

// ���������� �������
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

ATOM RegMyWindowClass(HINSTANCE, LPCTSTR);

// ������� ��������� ��������� WinMain
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow) {

    // ��� �������� ������
    LPCTSTR lpzClass = TEXT("My Window Class!");

    // ����������� ������
    if (!RegMyWindowClass(hInstance, lpzClass))
        return 1;

    // ���������� ��������� ������ ������
    RECT screen_rect;
    GetWindowRect(GetDesktopWindow(), &screen_rect); // ���������� ������
    int x = screen_rect.right / 2 - 400;
    int y = screen_rect.bottom / 2 - 300;

    // �������� ����������� ����
    HWND hWnd = CreateWindow(lpzClass, TEXT("Dialog Window"),
                             WS_OVERLAPPEDWINDOW | WS_VISIBLE, x, y, 800, 600, NULL, NULL,
                             hInstance, NULL);

    // ���� ���� �� �������, ��������� ����� ����� 0
    if (!hWnd) return 2;

    // ���� ��������� ����������
    MSG msg = {0};    // ��������� ���������
    int iGetOk = 0;   // ���������� ���������
    while ((iGetOk = GetMessage(&msg, NULL, 0, 0)) != 0) // ���� ���������
    {
        if (iGetOk == -1) return 3;  // ���� GetMessage ������ ������ - �����
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;  // ���������� ��� ���������� ���������
}

//////////////////////////////////////////////////////////////////////////
// ������� ����������� ������ ����
ATOM RegMyWindowClass(HINSTANCE hInst, LPCTSTR lpzClassName) {
    WNDCLASS wcWindowClass = {0};
    // ����� �-��� ��������� ���������
    wcWindowClass.lpfnWndProc = (WNDPROC) WndProc;
    // ����� ����
    wcWindowClass.style = CS_HREDRAW | CS_VREDRAW;
    // ���������� ���������� ����������
    wcWindowClass.hInstance = hInst;
    // �������� ������
    wcWindowClass.lpszClassName = lpzClassName;
    // �������� �������
    wcWindowClass.hCursor = LoadCursor(NULL, IDC_CROSS);
    // �������� ����� ����
    wcWindowClass.hbrBackground = (HBRUSH) COLOR_APPWORKSPACE;
    return RegisterClass(&wcWindowClass); // ����������� ������
}

//////////////////////////////////////////////////////////////////////////
// ������� ��������� ���������
LRESULT CALLBACK WndProc(
        HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    short xPos = GET_X_LPARAM(lParam);
    short yPos = GET_Y_LPARAM(lParam);
    // ������� � ��������� ���������
    switch (message) {
        case WM_MOUSEMOVE: {

            if (wParam = MK_LBUTTON) {
                std::cout << "x = " << xPos << "; y = " << yPos << std::endl;
            }
            break;
        }
        case WM_LBUTTONDOWN:
            HDC hDC; //������ �������� ����������
            PAINTSTRUCT ps; //������ ��������� ��������� ������������ ������
            HBRUSH hBrush;
            HPEN hPen;
            hDC=BeginPaint(hWnd, &ps);
            SetPixel(hDC, 10,10, RGB(0,0,0));
            ValidateRect(hWnd, NULL);
            EndPaint(hWnd, &ps);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);  // ������� �� ���������
            break;
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_DOWN: {
                    PostMessage(hWnd, WM_DESTROY, 0, 0);
                }
            }
            break;
        }
        default: {
            // ��� ��������� �� ������������ ���� ���������� ���� Windows
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    return 0;
}
